# CHANGELOG
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [3.1.16+9]
* Removed `ENGINEER` PV from `ioc.template`

### Other changes
* Removed old e3-specific PVs, and removed `calc` dependency due to the `scalcout` record.

## [3.1.16+2]

* Initial changelog

