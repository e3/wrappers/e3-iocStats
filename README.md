e3-iocStats
==

e3 wrapper for iocstats, a module which allows aggregation and polling of various IOC and system-related values.

For more information, see [iocStats](https://github.com/epics-modules/iocStats).